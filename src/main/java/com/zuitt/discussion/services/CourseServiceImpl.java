package com.zuitt.discussion.services;

import com.zuitt.discussion.models.Course;
import com.zuitt.discussion.repositories.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class CourseServiceImpl implements CourseService {
    @Autowired
    CourseRepository courseRepository;

    @Override

    public ResponseEntity createCourse(Course course) {
        course.setActive(true);
        courseRepository.save(course);
        return new ResponseEntity<>("Course Created!", HttpStatus.CREATED);
    }

    @Override
    public Iterable<Course> getAllCourses() {
        return courseRepository.findAll();
    }

    @Override
    public ResponseEntity updateCourse(Course course, Long id) {
        Course oldPost =courseRepository.findById(id).get();
        oldPost.setName(course.getName());
        oldPost.setDescription(course.getDescription());
        oldPost.setPrice(course.getPrice());

        courseRepository.save(oldPost);
        return new ResponseEntity<>("Course Updated Successfully", HttpStatus.OK);
    }

    @Override
    public ResponseEntity deleteCourse(Long id) {
        Course postFound=courseRepository.findById(id).get();
        postFound.setActive(false);
        courseRepository.save(postFound);
        return new ResponseEntity<>("Course Deleted Successfully", HttpStatus.OK);
    }
    public ResponseEntity getCourse(Long id){
        Course course=courseRepository.findById(id).get();
        return  new ResponseEntity(course,HttpStatus.OK);
    }

    @Override
    public ResponseEntity Unarchive(Long id) {
        Course course=courseRepository.findById(id).get();
        course.setActive(true);
        courseRepository.save(course);
        return new ResponseEntity("Course has been unarchived/recovered", HttpStatus.OK);
    }

    @Override
    public Iterable<Course> getAllActiveCourses() {
        return courseRepository.findAllByIsActive(true);
    }

}
