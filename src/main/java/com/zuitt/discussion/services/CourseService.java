package com.zuitt.discussion.services;

import com.zuitt.discussion.models.Course;
import org.springframework.http.ResponseEntity;

public interface CourseService {
    ResponseEntity createCourse(Course course);
    Iterable<Course> getAllCourses();
    ResponseEntity updateCourse(Course course,Long id);
    ResponseEntity deleteCourse(Long id);
    ResponseEntity getCourse(Long id);

    ResponseEntity Unarchive(Long id);

    Iterable<Course>getAllActiveCourses();
}
