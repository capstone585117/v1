package com.zuitt.discussion.services;

import com.zuitt.discussion.models.CourseEnrollment;
import org.springframework.http.ResponseEntity;

public interface CourseEnrollmentService {
    ResponseEntity enrollStudent(String stringToken,long courseid);
    Iterable<CourseEnrollment> getAllEnrollments();
}
