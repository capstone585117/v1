package com.zuitt.discussion.services;

import com.zuitt.discussion.config.JwtToken;
import com.zuitt.discussion.models.Course;
import com.zuitt.discussion.models.CourseEnrollment;
import com.zuitt.discussion.models.User;
import com.zuitt.discussion.repositories.CourseEnrollmentRepository;
import com.zuitt.discussion.repositories.CourseRepository;
import com.zuitt.discussion.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service

public class CourseEnrollmentServiceImpl implements CourseEnrollmentService{
    @Autowired
    UserRepository userRepository;
    @Autowired
    CourseRepository courseRepository;
    @Autowired
    CourseEnrollmentRepository courseEnrollmentRepository;
    @Autowired
    JwtToken jwtUtil;


    @Override
    public ResponseEntity enrollStudent(String stringToken, long courseid) {
        String studentName =jwtUtil.getUsernameFromToken(stringToken);
        User user= userRepository.findByUsername(studentName);
        Course course= courseRepository.findById(courseid).get();
        CourseEnrollment courseEnrollment= new CourseEnrollment("aa","ss");
        courseEnrollment.setUser(user);
        courseEnrollment.setCourse(course);
        courseEnrollment.setDateTime(LocalDateTime.now());
        courseEnrollmentRepository.save(courseEnrollment);
        return new ResponseEntity("Student Enrolled", HttpStatus.OK);
    }

    @Override
    public Iterable<CourseEnrollment> getAllEnrollments() {
       return courseEnrollmentRepository.findAll();
    }
}
