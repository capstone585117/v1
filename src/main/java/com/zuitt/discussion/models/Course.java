package com.zuitt.discussion.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.repository.CrudRepository;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
@Table(name="courses")
public class Course {

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private String name;
    @Column
    private String description;

    @Column
    private double price;
    @OneToMany(mappedBy ="course")
    @JsonIgnore
    Set<CourseEnrollment> enrollees;






    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    @Column
    private boolean isActive;

    public Set<CourseEnrollment> getEnrollees() {
        return enrollees;
    }

    public void setEnrollees(Set<CourseEnrollment> enrollees) {
        this.enrollees = enrollees;
    }





}
