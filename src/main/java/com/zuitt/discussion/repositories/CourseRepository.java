package com.zuitt.discussion.repositories;

import com.zuitt.discussion.models.Course;
import com.zuitt.discussion.models.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseRepository extends CrudRepository<Course,Object> {


    //SELECT * FROM `courses` WHERE is_active=true;
//    @Query(value = "SELECT * FROM Courses c WHERE c.is_active = ?1",
//            nativeQuery = true)
    Iterable<Course> findAllByIsActive(boolean status);

}
